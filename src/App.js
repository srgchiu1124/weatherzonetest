// import logo from './logo.svg';
import React, { useEffect, useState } from 'react';
import { format } from "date-fns";
import './App.css';
import axios from 'axios';

function App() {
  const codes = [
    12495,
    9388,
    3928,
    11,
    15465,
    5594,
    13896,
    624
  ];
  const [data, setData] = useState([]);
  const [selectedLocation, setLocation] = useState();

  useEffect(() => {  
    if (data.length === 0) {
      codes.forEach(code => {
        const url = 'https://ws.weatherzone.com.au/?lt=aploc&lc='+code+'&locdet=1&latlon=1&pdf=twc(period=48,detail=2)&u=1&format=json'
          const fetchData = async () => {
            const response = await axios(
              url
            );
            setData(prev => [...prev, response.data.countries[0].locations[0]]);
          }
        fetchData();
     })
    }
    
  },[selectedLocation]);
  
  const onLocationChange = (input) => {
    setLocation(input.currentTarget.value);
  }

  const test = () => {
    setLocation("");
  }

  return (
    <div className="App">
      <h2>WEATHERZONE</h2>
      <datalist id="locations" onSelect={test}>
        <select type="text" list="locations" onInput={onLocationChange} onSelect >
            {data.map(loc => <option key={loc.name}>{loc.name}</option>)}
        </select>
      </datalist>

      <div className="container tbl-container">

      <div className="table">
          <div className="row header">
              <div>Location</div>
              <div>Date</div>
              <div>Time</div>
              <div>Timezone</div>
              <div>Precipitation Probabilty</div>
              <div>Temperature</div>
              <div>Wind Speed</div>
              <div>Wind Direction</div>
          </div>
      </div>
          {data.filter(location =>  {
            if (selectedLocation === undefined || selectedLocation === "") {
              return data;
            }
            return location.name === selectedLocation
          }).map(loc =>{
            return <div className="table">{loc.part_day_forecasts.forecasts.map(forecast => {
              
              return <div className="row">
                  <div>{loc.name}</div>
                  <div>{format(new Date(forecast.local_time), "do MMM yyyy")}</div>
                  <div>{format(new Date(forecast.local_time), "H:mma")}</div>
                  <div>{forecast.time_zone}</div>
                  <div>{forecast.rain_prob}</div>
                  <div>{forecast.temperature}</div>
                  <div>{forecast.wind_speed}</div>
                  <div>{forecast.wind_direction_compass}</div>
                </div>
            })} </div>
              })}

      </div>


    </div>
  );
}

export default App;
